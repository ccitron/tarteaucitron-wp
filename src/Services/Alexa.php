<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Alexa
 * @property string account_id
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Alexa extends Service {

    public string $label = 'Alexa';

    public string $category = 'Mesure d\'audience';

    public array $options = [
        'account_id' => [
            'label' => 'ID Compte',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.alexaAccountID = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('alexa');",
            esc_js( $this->account_id ),
        );
    }
}
