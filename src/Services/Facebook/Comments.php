<?php

namespace CouleurCitron\TarteaucitronWP\Services\Facebook;

use CouleurCitron\TarteaucitronWP\Services\Service;

class Comments extends Service {

    public string $label = 'Facebook Comments';

    public string $category = 'Commentaire';

    public function script(): string {
        return "(tarteaucitron.job = tarteaucitron.job || []).push('facebookcomment');";
    }
}
