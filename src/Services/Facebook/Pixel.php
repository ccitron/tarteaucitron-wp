<?php

namespace CouleurCitron\TarteaucitronWP\Services\Facebook;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class FacebookPixel
 * @property mixed pixel_id
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Pixel extends Service {

    public string $label = 'Facebook Pixel';

    public string $category = 'Réseaux Sociaux';

    public array $options = [
        'pixel_id' => [
            'label' => 'ID',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.facebookpixelId = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('facebookpixel');",
            esc_js( $this->pixel_id ),
        );
    }
}
