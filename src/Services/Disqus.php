<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Disqus
 * @property string shortname
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Disqus extends Service {

    public string $label = 'Disqus';

    public string $category = 'Commentaire';

    public array $options = [
        'shortname' => [
            'label' => 'ID Site (shortname)',
        ],
    ];

    public function script(): string {
        return sprintf(
            "var disqus_shortname = '%s';\n" .
            "/* * * DON'T EDIT BELOW THIS LINE * * */\n" .
            "(function() {\n" .
            "var dsq = document.createElement('script');\n" .
            "dsq.type = 'text/javascript';\n" .
            "dsq.async = true;\n" .
            "dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';\n" .
            "(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);\n" .
            "})();",
            esc_js( $this->shortname ),
        );
    }
}
