<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Woopra
 * @property string domain
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Woopra extends Service {

    public string $label = 'Woopra';

    public string $category = 'Mesure d\'audience';

    public array $options = [
        'domain' => [
            'label' => 'Domaine',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.woopraDomain = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('woopra');",
            esc_js( $this->domain ),
        );
    }
}
