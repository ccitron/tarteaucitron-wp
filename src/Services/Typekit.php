<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Typekit
 * @property string typekit_id
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Typekit extends Service {

    public string $label = 'Adobe Typekit';

    public string $category = 'APIs';

    public array $options = [
        'typekit_id' => [
            'label' => 'ID Typekit',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.typekitId = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('typekit');",
            esc_js( $this->typekit_id ),
        );
    }
}
