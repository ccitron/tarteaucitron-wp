<?php

namespace CouleurCitron\TarteaucitronWP\Services;

abstract class Service implements \JsonSerializable {

    public string $name;

    public string $label;

    public string $category;

    public array $options = [];

    public bool $active = false;

    /**
     * Service constructor.
     */
    public function __construct() {
        $this->name = static::buildName( static::class );

        $data         = $this->getSavedData();
        $this->active = $data['active'] ?? false;
        foreach ( ( $data['options'] ?? [] ) as $optionKey => $option ) {
            if ( array_key_exists( 'value', $option ) ) {
                $this->$optionKey = $option['value'];
            }
        }
    }

    /**
     * @param class-string $class
     */
    public static function buildName( string $class ): string {
        return str_replace( __NAMESPACE__ . '\\', '', $class );
    }

    public static function getClassFromName( $name ): string {
        return __NAMESPACE__ . '\\' . $name;
    }

    abstract public function script(): string;

    public function save(): bool {
        $services                = $this->getSavedData();
        $services[ $this->name ] = $this->toArray();

        return update_option( 'tacwp_services', json_encode( $services ) );
    }

    public function toArray(): array {
        return [
            'name'     => $this->name,
            'label'    => $this->label,
            'category' => $this->category,
            'options'  => (object) $this->options,
            'active'   => $this->active,
        ];
    }

    public function toJson(): string {
        return json_encode( $this->jsonSerialize() );
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize() {
        return $this->toArray();
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->toJson();
    }

    /**
     * @return mixed|null
     */
    public function __get( string $name ) {
        return $this->options[ $name ]['value'] ?? null;
    }

    /**
     * @param mixed $value
     */
    public function __set( string $name, $value ) {
        $this->options[ $name ]['value'] = $value;
    }

    public function __isset( $name ) {
        return isset( $this->options[ $name ]['value'] );
    }

    protected function getSavedData(): array {
        $services = json_decode( get_option( 'tacwp_services' ), JSON_OBJECT_AS_ARRAY ) ?: [];

        return $services[ $this->name ] ?? [];
    }
}
