<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Clicky
 * @property string clicky_id
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Clicky extends Service {

    public string $label = 'Click';

    public string $category = 'Mesure d\'audience';

    public array $options = [
        'clicky_id' => [
            'label' => 'ID',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.clickyId = '%s';\ntarteaucitron.user.clickyMore = function () { /* add here your optionnal clicky function */ };\n(tarteaucitron.job = tarteaucitron.job || []).push('clicky');",
            esc_js( $this->clicky_id ),
        );
    }
}
