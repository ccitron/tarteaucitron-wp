<?php

namespace CouleurCitron\TarteaucitronWP\Services;

class TimelineJS extends Service {

    public string $label = 'Timeline JS';

    public string $category = 'APIs';

    public function script(): string {
        return "(tarteaucitron.job = tarteaucitron.job || []).push('timelinejs');";
    }
}
