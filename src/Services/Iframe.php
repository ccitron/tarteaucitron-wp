<?php

namespace CouleurCitron\TarteaucitronWP\Services;

/**
 * Class Iframe
 * @property string iframe_name
 * @property string url
 * @property string cookies
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Iframe extends Service {

    public string $label = 'Iframe';

    public string $category = 'Autre';

    public array $options = [
        'iframe_name' => [
            'label' => 'Nom',
        ],
        'url'         => [
            'label' => 'URL',
        ],
        'cookies'     => [
            'label'       => 'Cookies',
            'placeholder' => 'Séparer par des virgules',
        ],
    ];

    public function script(): string {
        return sprintf(
            "var tarteaucitron_interval = setInterval(function() {\n" .
            "if (typeof tarteaucitron.services.iframe.name == 'undefined') {\n" .
            "return;\n" .
            "}\n" .
            "clearInterval(tarteaucitron_interval);\n" .
            "tarteaucitron.services.iframe.name = '%s';\n" .
            "tarteaucitron.services.iframe.uri = '%s';\n" .
            "tarteaucitron.services.iframe.cookies = '%s';\n" .
            "}, 10);\n" .
            "(tarteaucitron.job = tarteaucitron.job || []).push('iframe');",
            esc_js( $this->iframe_name ),
            esc_js( $this->url ),
            esc_js( explode( ',', $this->cookies ) ),
        );
    }
}
