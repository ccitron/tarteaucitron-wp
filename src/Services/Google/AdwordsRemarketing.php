<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class AdwordsRemarketing
 * @property string $adwords_id
 * @package CouleurCitron\TarteaucitronWP\Services\Google
 */
class AdwordsRemarketing extends Service {

    public string $label = 'Google Adwords (remarketing)';

    public string $category = 'Régie publicitaire';

    public array $options = [
        'adwords_id' => [
            'label' => 'ID',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.adwordsremarketingId = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('googleadwordsremarketing');",
            esc_js( $this->adwords_id ),
        );
    }
}
