<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class GoogleMaps
 * @property string api_key
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class Maps extends Service {

    public string $label = 'Google Maps';

    public string $category = 'APIs';

    public array $options = [
        'api_key' => [
            'label' => 'Clé API',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.googlemapsKey = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('googlemaps');",
            esc_js( $this->api_key ),
        );
    }

}
