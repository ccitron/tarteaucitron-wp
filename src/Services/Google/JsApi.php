<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

class JsApi extends Service {

    public string $label = 'Google JsApi';

    public string $category = 'APIs';

    public function script(): string {
        return "(tarteaucitron.job = tarteaucitron.job || []).push('jsapi');";
    }

}
