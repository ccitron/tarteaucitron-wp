<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class GoogleAnalyticsUniversal
 * @property string $ua_code
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class AnalyticsUniversal extends Service {

    public string $label = 'Google Analytics (universal)';

    public string $category = 'Mesure d\'audience';

    public array $options = [
        'ua_code' => [
            'label' => 'Code UA',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.analyticsUa = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('analytics');",
            esc_js( $this->ua_code ),
        );
    }
}
