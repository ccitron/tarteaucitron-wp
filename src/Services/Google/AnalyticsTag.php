<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class AnalyticsTag
 * @property string $ua_code
 * @package CouleurCitron\TarteaucitronWP\Services\Google
 */
class AnalyticsTag extends Service {

    public string $label = 'Google Analytics (gtag.js)';

    public string $category = 'Mesure d\'audience';

    public array $options = [
        'ua_code' => [
            'label' => 'Code UA',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.gtagUa = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('gtag');",
            esc_js( $this->ua_code ),
        );
    }
}
