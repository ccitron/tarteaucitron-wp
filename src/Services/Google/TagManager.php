<?php

namespace CouleurCitron\TarteaucitronWP\Services\Google;

use CouleurCitron\TarteaucitronWP\Services\Service;

/**
 * Class GoogleTagManager
 * @property string google_tag_manager_id
 * @package CouleurCitron\TarteaucitronWP\Services
 */
class TagManager extends Service {

    public string $label = 'Google Tag Manager';

    public string $category = 'APIs';

    public array $options = [
        'google_tag_manager_id' => [
            'label' => 'ID GTM',
        ],
    ];

    public function script(): string {
        return sprintf(
            "tarteaucitron.user.googletagmanagerId = '%s';\n(tarteaucitron.job = tarteaucitron.job || []).push('googletagmanager');",
            esc_js( $this->google_tag_manager_id ),
        );
    }
}
