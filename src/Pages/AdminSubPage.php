<?php

namespace CouleurCitron\TarteaucitronWP\Pages;

abstract class AdminSubPage extends AdminPage {

    protected string $parent_slug;

    public function __construct(
        string $parent_slug,
        string $page_title,
        string $menu_title,
        string $capability,
        string $slug,
    ) {
        parent::__construct( $page_title, $menu_title, $capability, $slug );

        $this->parent_slug = $parent_slug;
    }

    public function register(): string {
        return add_submenu_page(
            $this->parent_slug,
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->slug,
            [ $this, 'render' ],
        );
    }

}
