<?php

namespace CouleurCitron\TarteaucitronWP\Pages;

abstract class AdminPage {

    protected string $page_title;

    protected string $menu_title;

    protected string $capability;

    protected string $slug;

    protected string $icon;

    protected ?int $position;

    public function __construct(
        string $page_title,
        string $menu_title,
        string $capability,
        string $slug,
        string $icon = '',
        ?int $position = null,
    ) {
        $this->page_title = $page_title;
        $this->menu_title = $menu_title;
        $this->capability = $capability;
        $this->slug       = $slug;
        $this->icon       = $icon;
        $this->position   = $position;
    }

    public function register(): string {
        return add_menu_page(
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->slug,
            [ $this, 'render' ],
            $this->icon,
            $this->position
        );
    }

    abstract public function render(): void;

}
