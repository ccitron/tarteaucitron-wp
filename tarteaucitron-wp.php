<?php
/*
 * Plugin Name: tarteaucitron.js for WordPress
 * Description: Cookie manager WordPress plugin
 * Version:     0.2.5
 * Author:      Couleur Citron
 * Author URI:  https://wwww.couleur-citron.com
 * License:     MIT
 * Text Domain: tacwp
 */

if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

\CouleurCitron\TarteaucitronWP\Plugin::init();
