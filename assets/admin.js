import {createApp} from 'vue';
import Services from './Services';

const container = document.getElementById('app');

createApp(Services, {
    services: JSON.parse(container.dataset.services),
    action: container.dataset.action,
}).mount(container);
